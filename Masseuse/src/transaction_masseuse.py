'''
transaction_masseuse.py

Data masseuse. Takes in a masseuse configuration file and turns
data into a usable transactions for Frequent Pattern Mining. 
'''

import numpy as np

import json
import argparse
import csv

from masseusetools import *

import sys

'''
All the parsing happens here
'''

def parseBinary(col, block):
  # Globals
  global _NEWDATA
  global _NUMROWS

  # Load up the configs and run
  label = block['label']
  truefalse = block['truefalse']
  for i in range(0,_NUMROWS):
    # See if the value is the "true" value
    if col[i] == truefalse[0]:
      _NEWDATA[i].append(label)

def parseCategorical(col, block):
  # Globals
  global _NEWDATA
  global _NUMROWS

  # Load up the configs and run
  labels = block['labels']
  values = block['values']
  for i in range(0,_NUMROWS):
    # Get the index of the value from values
    index = values.index(col[i])
    # Then get the appropriate label
    _NEWDATA[i].append(labels[index])

def parseRange(col, block):
  # Globals
  global _NEWDATA
  global _NUMROWS

  # Load up the configs and run the ranges
  ranges = block['ranges']
  for rangegroup in ranges:
    for rangevalue in rangegroup['rangevalues']:
      label = rangevalue['label']
      low = rangevalue['range']['from'] if 'from' in rangevalue['range'] else None
      high = rangevalue['range']['to'] if 'to' in rangevalue['range'] else None
      # Check each value in the column to see if it's in this range
      tempcol = npinRange(col, low, high)
      for i in range(0, len(tempcol)):
        # If it's in the range, add the label
        if tempcol[i]:
          _NEWDATA[i].append(label)

'''
Main runner function
'''

_NEWDATA = None
_NUMROWS = None

def main(f_data,f_config):
  # Open up the config and get the data
  config = json.load(f_config)
  data = readdata(f_data, config['header'], config['footer'], config['hasid'], config['delimiter'])

  # Prep the new data holder. Cannot use numpy anymore
  global _NEWDATA
  global _NUMROWS
  _NEWDATA = []
  _NUMROWS = len(data)
  for i in range(0,_NUMROWS):
    _NEWDATA.append([])

  for block in config['structure']:
    print(block['name'])
    col = data[block['name']]
    # Get rid of bytes, convert to normal strings
    if npisbytes(col).all():
      col = npbytestostring(col)
    
    if block['type'] == 'binary':
      parseBinary(col,block)
      continue
    
    if block['type'] == 'categorical':
      parseCategorical(col,block)
      continue
    
    if block['type'] == 'range':
      parseRange(col,block)
      continue

  filename = "{0}.masseuse.csv".format(config['name'])
  with open(filename,'w') as outfile:
    writer = csv.writer(outfile, delimiter=' ', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerows(_NEWDATA)
  
  print()
  print("Your file has been created as {0}".format(filename))


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Create a transaction file from a dense csv and a configuration file")
  parser.add_argument("data", help="Data file", type=argparse.FileType('r'))
  parser.add_argument("config", help="Metadata config file", type=argparse.FileType('r'))

  args = parser.parse_args()

  data = args.data
  config = args.config

  main(data,config)
