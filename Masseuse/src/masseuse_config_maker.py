'''
masseuse_config_maker.py

Data masseuse config creator. Supply it a raw csv of your data and it will
guide you through the creation of a config file.
'''

import numpy as np
from scipy import stats

import argparse
import json

from masseusetools import *

import sys

_MANUAL = True
_CONSERVATIVE = True

# Checks to see if there are only two values provided
def checkForBinary(col,name):
  global _MANUAL

  # Grab the labels by getting the unique values
  u,c = np.unique(col,return_counts=True)
  labels = u.tolist()
  counts = c.tolist()

  # If there are only two labels, assume binary
  if len(labels) == 2:
    # Order the labels by size
    indicies = [counts.index(c.min()),counts.index(c.max())]
    labels = [labels[indicies[0]],labels[indicies[1]]]
    # If we're running in manual, check it's binarity and get the preferered label
    if _MANUAL:
      isreallybinary = inputselection("Is this a binary value? y/n",["y","n"])
      if isreallybinary:
        mainlabel = inputselection("Which value do you want to show up? "+
          "{0}".format("/".join(labels)),labels)

        mainlabel = "{0}-{1}".format(name,mainlabel)
        
        # Create the truefalse list
        truefalse = [mainlabel]
        for label in labels:
          if label not in truefalse:
            truefalse.append(label) 
        return (True, mainlabel, truefalse)
      return (False, None, None)
    
    # If not, just grab it in the order it came
    else:
      return (True, "{0}-{1}".format(name,labels[0]), labels)

  return (False, None, None)

# Checks to see if there are few enough distinct values to make for categorical
def checkForCategorical(col, name, force=False):
  # Grab the labels by getting the unique values
  values = np.unique(col)
  # If there are 1/10th as many or fewer labels than there are transactions
  # we assume it's categorical
  # TODO Come up with a better solution to this that is still automated
  if len(values) < col.size * 0.1 or npisstring(values).all() or force:
    labels = npappendname(values,name)
    return (True, values.tolist(), labels.tolist())
  else:
    return (False, None, None)

# Creates a +/- range around the median as well as a 4 quartile range
def createDefaultRanges(col, name):
  # Define the ranges
  ranges = []
  
  coarse = {}
  coarse['rangename'] = "coarse_grained"
  coarse['rangevalues'] = []
  
  fine = {}
  fine['rangename'] = "fine_grained"
  fine['rangevalues'] = []

  # Check to make sure there's actually a range
  min = col.min()
  max = col.max()
  if min == max:
    return None

  # Get coarse ranges
  median = np.median(col)
  lessThanMedian = {}
  lessThanMedian['label'] = "{0}-lessThan{1}".format(name,median)
  lessThanMedian['range'] = {}
  lessThanMedian['range']['to'] = median

  greaterThanMedian = {}
  greaterThanMedian['label'] = "{0}-{1}plus".format(name,median)
  greaterThanMedian['range'] = {}
  greaterThanMedian['range']['from'] = median

  coarse['rangevalues'].append(lessThanMedian)
  coarse['rangevalues'].append(greaterThanMedian)

  # Get fine ranges
  quantiles = stats.mstats.mquantiles(col)
  lowest = {}
  lowest['label'] = "{0}-lessThan{1}".format(name,quantiles[0])
  lowest['range'] = {}
  lowest['range']['to'] = quantiles[0]

  low = {}
  low['label'] = "{0}-{1}to{2}".format(name,quantiles[0],quantiles[1])
  low['range'] = {}
  low['range']['from'] = quantiles[0]
  low['range']['to'] = quantiles[1]

  high = {}
  high['label'] = "{0}-{1}to{2}".format(name,quantiles[1],quantiles[2])
  high['range'] = {}
  high['range']['from'] = quantiles[1]
  high['range']['to'] = quantiles[2]

  highest = {}
  highest['label'] = "{0}-{1}plus".format(name,quantiles[2])
  highest['range'] = {}
  highest['range']['from'] = quantiles[2]

  fine['rangevalues'].append(lowest)
  fine['rangevalues'].append(low)
  fine['rangevalues'].append(high)
  fine['rangevalues'].append(highest)

  # Add them both to the ranges
  # ranges.append(coarse) # Removing for now
  ranges.append(fine)

  return ranges

'''
Autogeneration functions
'''

def autogen(data):
  global _CONSERVATIVE
  print("Automatically generating configuration file")

  configs = []
  tempdata = None

  for name in data.dtype.names:
    col = data[name]

    # Strip NaNs : http://stackoverflow.com/questions/11620914/removing-nan-values-from-an-array
    col = [~np.isnan(col)]

    # Check if it's int or float
    b_isnum = col.dtype is np.dtype("int_") or col.dtype is np.dtype("float_")

    block = {"type": None, "name":name}

    if b_isnum and np.isnan(np.sum(col)):
      block["hasempty"] = True
      col = np.array([x for x in col if not np.isnan(x)])
    
    # Get rid of bytes, convert to normal strings
    if not b_isnum and npisbytes(col).all():
      col = npbytestostring(col)

    if np.unique(col).size == 1:
      # There is only a single value in this column. Ignore
      continue

    # If we're running in conservative mode, don't do binary check
    if not _CONSERVATIVE:
      # Check to see if this is a binary column
      (b_isbinary, label, orderedlabels) = checkForBinary(col,name)
      if b_isbinary:
        block["type"] = "binary"
        block["label"] = label
        block["truefalse"] = orderedlabels
        configs.append(block)
        continue

    # Check to see if this is a categorical column
    (b_iscategorical, values, labels) = checkForCategorical(col,name)
    if b_iscategorical:
      block["type"] = "categorical"
      block["labels"] = labels
      block["values"] = values
      configs.append(block)
      continue

    # It is not binary or categorical, so let's get some summary stats
    if b_isnum:
      block["type"] = "range"
      block["ranges"] = createDefaultRanges(col,name)
      if block['ranges'] is None:
        continue
      configs.append(block)
      continue

  return configs

'''
Manual generation functions
'''

def manualgen(data):
  print("Manually generating configuration file")

'''
Main runner function
'''

def main(infile):
  global _MANUAL
  global _CONSERVATIVE

  print("First, let's get some information about your project")

  pname = None
  while pname is None:
    pname = masseuse_input("What is the name of your project?\n")
    if pname != '':
      break

  print("Thank you. Now let's get some information about your file")

  # See if the data file has header rows
  header = masseuse_input("How many header rows are there? Default 1\n")
  if header == '':
    header = 1

  # See if the data file has footer rows
  footer = masseuse_input("How many footer rows are there? Default 0\n")
  if footer == '':
    footer = 0

  # See if there is an ID column
  hasid = masseuse_input("How many id columns are there before your data? Default: 1\n")
  if hasid == '':
    hasid = 1

  # Get the delimiter
  delim = masseuse_input("What is your delimiter? Default: ,\n")
  if delim == '':
    delim = ','
  
  # Prep the block object
  config = {}
  config['name'] = pname
  config['header'] = header
  config['footer'] = footer
  config['hasid'] = hasid
  config['delimiter'] = delim
  config['structure'] = None

  # Get the data
  data = readdata(infile,header,footer,hasid,delim)

  # Check to see if we're running in auto or in manual
  auto = inputselection("Would you like Masseuse config to try and automatically create a "+
    "configuration file for you? y/n",["y","n"])
  if auto == 'y':
    _MANUAL = False
    conservative = inputselection("Would you like to be conservative in your automation? y/n\n" +
      "(Categorical and Range only)", ["y","n"])
    if conservative == 'y':
      _CONSERVATIVE = True
    else:
      _CONSERVATIVE = False
    config['structure'] = autogen(data)
  
  # We're running in manual mode
  else:
    _MANUAL = True
    config['structure'] = manualgen(data)

  fname = "{0}.masseuse".format(pname)
  f = open(fname,'w')
  json.dump(config,f,indent=4,separators=(',',': '))
  f.close()

  print("Your configuration file has been made and is located here:\n{0}".format(fname))

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description="Interactively creates a masseuse config file "+
    "from your data")
  parser.add_argument("data",help="Your data file",type=argparse.FileType('r'))

  args = parser.parse_args()

  infile = args.data

  main(infile)
