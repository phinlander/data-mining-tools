'''
masseusetools.py

Utility tools used in the Masseuse data massaging toolkit.
'''

'''
Type checking functions
'''

import numpy as np
import sys

# Checks if a value/array is binary 0s and 1s
def isbinary(value):
  try:
    return int(value) == 0 or int(value) == 1
  except ValueError:
    return False

npisbinary = np.vectorize(isbinary)

# Checks if a value/array is int only
def isint(value):
  try:
    int(value)
    return True
  except ValueError:
    return False

npisint = np.vectorize(isint)

# Checks if a value/array is float only
def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

npisfloat = np.vectorize(isfloat)

# Checks if a value/array is a byte array (for byte to string conversion)
def isbytes(value):
  return type(value) is bytes

npisbytes = np.vectorize(isbytes)

# Check if a value/array is a string
def isstring(value):
  return type(value) is str

npisstring = np.vectorize(isstring)


'''
Type conversion functions
'''

# Converts to binary based on given label
def tobinary(value,label):
  return value.lower() == label.lower()

nptobinary = np.vectorize(tobinary)

# Converts to int
def toint(value):
  return int(value)

nptoint = np.vectorize(toint)

# Converts to float
def tofloat(value):
  return float(value)

nptofloat = np.vectorize(tofloat)

# Converts from b'asdf' to 'asdf'
def bytestostring(value):
  return value.decode()

npbytestostring = np.vectorize(bytestostring)

# Checks to see if there are only ints and converts to ints if true
def checkForInt(col):
  b_isint = npisint(col).all()
  if b_isint:
    return (b_isint, nptoint(col))
  else:
    return (b_isint, None)

# Checks to see if there are only floats and converts to floats if true
def checkForFloat(col):
  b_isfloat = npisfloat(col).all()
  if b_isfloat:
    return (b_isfloat, nptofloat(col))
  else:
    return (b_isfloat, None)

# Append name to values to make new labels
def appendname(value, name):
  return "{0}-{1}".format(name,value)

npappendname = np.vectorize(appendname)

# Run the range finder - [low,value,high)
def inRange(value,low=None,high=None):
  if np.isnan(value):
    return False
  if low is None:
    if high is None:
      raise Exception("You have to include a floor or a ceiling for range")
    return value < high # No low, so just have to be less than the high
  if high is None:
    return value >= low # No high, so just have to be greater than or equal to the low
  return value >= low and value < high # Both high and low, so you have to be in the middle

npinRange = np.vectorize(inRange)


'''
Data IO
'''

def readdata(infile, header, footer, hasid, delim):
  # If there's a header, read it in for the names
  names = None
  if header > 0:
    header -= 1
    names = True

  # Read in the data as a numpy array
  data = np.genfromtxt(infile.name,
    skip_header=header,
    skip_footer=footer,
    delimiter=delim,
    names=names,
    dtype=None,
    autostrip=True)

  if hasid:
    names = list(data.dtype.names[hasid:])
    data = data[names].copy()
  return data


'''
User input/output
'''

def masseuse_input(message):
  if sys.version_info[0] == 2:
    return raw_input(message)
  else:
    return input(message)

def inputselection(message, choices):
  response = None
  choices = [x.lower() for x in choices]
  while response is None:
    response = masseuse_input("{0}\n".format(message)).lower()
    if response in choices:
      return response
    else:
      print("Please provide a valid choice")
      response = None
