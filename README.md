# Data Mining Tools
---

## Install Scripts
Scripts in this folder can be used to install a variety of tools on CentOS6 systems. CentOS 7 coming soon.

### Available
#### CentOS 6
* Scala

#### CentOS 7
Coming Soon...

### In Development
#### CentOS 6
* CDH
* Apache Drill
* Apache Zeppelin

#### CentOS 7
* Jenkins

---

## Data Masseuse
First tool to develop. Takes an arbitrary .csv file, with or without headers, and allows the user to specify different data types for each column. Currently support data types are:
* Categorical - Example: Red/Green/Blue
* Binary - Example: 0/1, M/F
* Range - Example: 0-9/10-19/20-29. Multiple ranges allowed.

### Categorical
Categorical data is just what it sounds like: categorical. Age, race, species, color, anything that has a small, discrete range when compared to the size of the data.

### Binary
Binary data is usually represented in the data as a 0 or a 1, T or an F, or True or False. The tool will by default check for the three binary types specified, but a user can also supply their own binary values using "truefalse": ["true","false"]. When run this will remove all false instances and convert all true instances into the value specified by "label".

### Range
Range types are used for continuous data and allow for more complex use cases. Ranges, in the mathematical sense, should be thought of as

if x \existsin [A,B), then label

meaning if x >= A && x < B is true, then the specified label is used. If A or B is blank, then one side of the if statement is ignored. For example, [,10] will check if x < 10, and if true use that label.

A Range type may also contain multiple ranges, which is useful when working with such things as age, weight, height, or any other "continuous" value which you want to discretize. Because of this, even single ranges exist in an array value, as shown in the example below.

### Example config file
```json
{
	"header": 1,
	"delimiter": ",",
	"name": "smoking",
	"structure": [
		{
			"label": "F",
			"name": "sex",
			"type": "binary",
			"truefalse": ["F", "M"]
		},
		{
			"label": "smoker-1",
			"name": "smoker",
			"type": "binary",
			"truefalse": [1, 0]
		},
		{
			"name": "age",
			"type": "range",
			"ranges": [
				{
					"rangename": "coarse_grained",
					"rangevalues": [
						{
							"label": "age-lessThan54.0",
							"range": {"to": 54.0}
						}, {
							"label": "age-54.0plus",
							"range": {"from": 54.0}
						}
					]
				}, {
					"rangename": "fine_grained",
					"rangevalues": [
						{
							"label": "age-lessThan32.7",
							"range": {"to": 32.7}
						}, {
							"label": "age-32.7to54.0",
							"range": {"from": 32.7, "to": 54.0}
						}, {
							"label": "age-54.0to78.3",
							"range": {"from": 54.0, "to": 78.3}
						}, {
							"label": "age-78.3plus",
							"range": {"from": 78.3}
						}
					]
				}
			]
		}, {
		    "values": ["black", "blonde", "brown", "red"],
			"labels": ["hair_color-black", "hair_color-blonde", "hair_color-brown", "hair_color-red"],
			"name": "hair_color",
			"type": "categorical"
		}
	],
	"footer": 0
}
```

### Example data
```csv
sex, smoker, age, hair color
M, 1, 25, brown
M, 0, 36, red
F, 0, 86, black
M, 0, 75, brown
F, 1, 54, blonde
```

__Returns__
```csv
smoker-1 agelessThan54.0 age-lessThan32.7 hair_color-brown
agelessThan54.0 age-32.7to54.0 hair_color-red
sex-F age-54.0plus age-78.3plus hair_color-black
age-54.0plus age-54.0to78.3 hair_color-brown
sex-F smoker-1 age-54.0plus age-54.0to78.3 hair_color-blonde
```