#!/bin/sh

# Installs scala given a new and old version
# If only one version is given, it's assumed it's a fresh install
# If two versions are given, it's assumed you're updating scala
# If no version is given, it's assumed it's a frseh install of whatever's current as of the last time the script was updated

# Get first argument
if [ $1 ]
then
  VERSION=$1
else
  VERSION=2.11.7
fi

# Set scala directory name
SCALA_DIR=scala-$VERSION

# Get scala from typesafe site
wget http://downloads.typesafe.com/scala/$VERSION/$SCALA_DIR.tgz 

# Create Scala home path and add it to profile.d
SCALA_HOME=/usr/local/scala/$SCALA_DIR
echo "SCALA_HOME=$SCALA_HOME" > /etc/profile.d/scala.sh
echo "export SCALA_HOME" >> /etc/profile.d/scala.sh

# Create the scala path in /usr/local, if it doesn't exist
mkdir -p /usr/local/scala 2> /dev/null
mv $SCALA_DIR.tgz /usr/local/scala
cd /usr/local/scala
tar zxvf $SCALA_DIR.tgz
rm $SCALA_DIR.tgz

# If second arg is set, assume removal of an older install
if [ $2 ]
then
  REMOVE=/usr/local/scala/scala-$2
  update-alternatives --remove "scala" "$REMOVE/bin/scala"
  update-alternatives --remove "scalac" "$REMOVE/bin/scalac"
  update-alternatives --remove "scalap" "$REMOVE/bin/scalap"
  update-alternatives --remove "scaladoc" "$REMOVE/bin/scaladoc"
  update-alternatives --remove "fsc" "$REMOVE/bin/fsc"
fi

# Add to /usr/bin and updated the alternatives
update-alternatives --install "/usr/bin/scala" "scala" "$SCALA_HOME/bin/scala" 1
update-alternatives --install "/usr/bin/scalac" "scalac" "$SCALA_HOME/bin/scalac" 1
update-alternatives --install "/usr/bin/scalap" "scalap" "$SCALA_HOME/bin/scalap" 1
update-alternatives --install "/usr/bin/scaladoc" "scaladoc" "$SCALA_HOME/bin/scaladoc" 1
update-alternatives --install "/usr/bin/fsc" "fsc" "$SCALA_HOME/bin/fsc" 1
